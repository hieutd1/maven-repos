# README #

### On project level dir ###
add this code to **build.gradle** 


```
#!gradle

...
allprojects {
    repositories {
        ...
        maven {

            url "https://api.bitbucket.org/1.0/repositories/hieutd1/maven-repos/raw/master/"
        }
    }
}
...
```

Add below code if url need authenticate
```
#!gradle

...
def versionPropsFile = file(project.rootDir.absolutePath + '/bitbucket_login.properties')
def Properties versionProps = new Properties()

if (versionPropsFile.canRead()) {
    versionProps.load(new FileInputStream(versionPropsFile))

} else {
    throw new GradleException("Create bitbucket_login.properties on project root dir for store USERNAME and PASSWORD bitbucket's repository")
}
...
allprojects {
    repositories {
        ...
        maven {   
            credentials {
                username versionProps['USERNAME']
                password versionProps['PASSWORD']
            }
            url "https://api.bitbucket.org/1.0/repositories/hieutd1/maven-repos/raw/master/"
        }
    ...
    }
}
...
```



format of maven url for other repository

```
#!html

https://api.bitbucket.org/1.0/repositories/{bitbucket_username}/{repo_slug}/raw/{reversion_or_branch}/{path}
```


Create **bitbucket_login.properties** then ignore it from git **.ignore** for safe (only if url need authenticate)

```
#!java

USERNAME=<bitbucket_username>
PASSWORD=<bitbucket_password>
```